package com.kind.common.client.response;

/**
 * Json响应结果对象【针对最简单的返回结果】
 * 
 * @author 李明
 *
 */
public class JsonResponseResult {

	/** 响应业务状态码[0-成功 非0-失败] */
	private Integer ret;
	/** 响应消息 */
	private String msg;
	
	public JsonResponseResult(Integer ret, String msg) {
        this.ret = ret;
        this.msg = msg;
    }
	
	public JsonResponseResult(){
		
	}
	
	public static JsonResponseResult build(Integer ret, String msg) {
        return new JsonResponseResult(ret, msg);
    }
	
	public static JsonResponseResult success() {
        return new JsonResponseResult(0, "OK");
    }

	public Integer getRet() {
		return ret;
	}

	public void setRet(Integer ret) {
		this.ret = ret;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
