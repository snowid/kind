package com.kind.perm.core.account.dao.impl;

import com.kind.common.datasource.DataSourceKey;
import com.kind.common.datasource.KindDataSourceHolder;
import com.kind.common.persistence.PageQuery;
import com.kind.common.persistence.mybatis.BaseDaoMyBatisImpl;
import com.kind.perm.core.account.dao.AccountDao;
import com.kind.perm.core.account.domain.AccountDO;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * 用户数据访问实现类. <br/>
 * 
 * @date:2016年12月11日 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Repository
public class AccountDaoImpl extends BaseDaoMyBatisImpl<AccountDO, Serializable> implements AccountDao {

	@Override
	public List<AccountDO> page(PageQuery pageQuery) {
		return super.query(NAMESPACE + "page", pageQuery);
	}

	@Override
	public int count(PageQuery pageQuery) {
		return super.count(NAMESPACE + "count", pageQuery);
	}



	@Override
	public int saveOrUpdate(AccountDO entity) {
		KindDataSourceHolder.setDataSourceKey(DataSourceKey.YM_ORDER_DATA_SOURCE);
		if (entity.getId() == null) {
			return super.insert(NAMESPACE + "insert", entity);
		} else {
			return	super.update(NAMESPACE + "update", entity);
		}
	}

	@Override
	public void remove(Long id) {
		delete(NAMESPACE + "delete", id);
	}

	@Override
	public AccountDO getById(Long id) {
		return super.getById(NAMESPACE + "getById", id);
	}
	
	@Override
	public List<AccountDO> queryList(AccountDO entity) {
		return super.query(NAMESPACE + "queryList", entity);
	}


}
