<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title></title>
<%@ include file="/WEB-INF/views/include/easyui.jsp"%>
</head>

<body>
	<div data-options="region: 'center'">
		<div class="container">
			<div class="content">
				<form id="mainformCustom" action="${ctx}/tableCustom/tableCustom/${action}"
					method="post" novalidate class="form">

					<div class="tabs-panels">
						<div class="panel">
							<div title="" data-options="closable:false"
								class="basic-info panel-body panel-body-noheader panel-body-noborder">
								<div class="column">
									<span class="current">模板内容信息:</span>
								</div>
								<table class="kv-table" cellspacing="10">
									<input type="hidden" name="id" value="${entity.id }" />
									<input type="hidden" name="tbType" value="${entity.tbType }" />
									<tr>
										<td class="kv-label">顺序号：</td>
										<td class="kv-content">
											<input name="fieldSort" type="text" value="${entity.fieldSort}" class="easyui-validatebox"
											data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">表字段对应的javaBean属性名称：</td>
										<td class="kv-content">
											<input name="fieldName" type="text" value="${entity.fieldName}" class="easyui-validatebox"
											data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									
									<tr>
										<td class="kv-label">属性类型：</td>
										<td class="kv-content">
											<select id="fieldType" name="fieldType" style="width:180px">
												<c:if test="${entity.fieldType == '' or entity.fieldType == null }">
													<option value="1" selected> 文本</option>
													<option value="2">数字</option>
													<option value="3">日期 YYYY-MM-dd</option>
													<option value="4">日期YYYY-MM-dd HH:mm:ss</option>
												</c:if>
	                                            <c:if test="${entity.isExport == '1' }">
	                                            	<option value="1" selected> 文本</option>
													<option value="2">数字</option>
													<option value="3">日期 YYYY-MM-dd</option>
													<option value="4">日期YYYY-MM-dd HH:mm:ss</option>
	                                            </c:if>
	                                            <c:if test="${entity.isExport == '2' }">
	                                           		<option value="1"> 文本</option>
													<option value="2" selected>数字</option>
													<option value="3">日期 YYYY-MM-dd</option>
													<option value="4">日期YYYY-MM-dd HH:mm:ss</option>
	                                            </c:if>
	                                            <c:if test="${entity.isExport == '3' }">
	                                           		<option value="1"> 文本</option>
													<option value="2">数字</option>
													<option value="3" selected>日期 YYYY-MM-dd</option>
													<option value="4">日期YYYY-MM-dd HH:mm:ss</option>
	                                            </c:if>
	                                            <c:if test="${entity.isExport == '4' }">
	                                           		<option value="1"> 文本</option>
													<option value="2">数字</option>
													<option value="3" selected>日期 YYYY-MM-dd</option>
													<option value="4">日期YYYY-MM-dd HH:mm:ss</option>
	                                            </c:if>
											</select>
										</td>
									</tr>
									<tr>
										<td class="kv-label">默认的显示名：</td>
										<td class="kv-content">
											<input name="fieldTitle" type="text" value="${entity.fieldTitle}" class="easyui-validatebox"
											data-options="required:true,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">显示的别名，如果没有则显示默认名称：</td>
										<td class="kv-content">
											<input name="fieldAnotherTitle" type="text" value="${entity.fieldAnotherTitle}" class="easyui-validatebox"
											data-options="required:false,validType:['length[0,255]']" />
										</td>
									</tr>
									<tr>
										<td class="kv-label">是否导出：</td>
										<td class="kv-content">
											<select id="isExport" name="isExport" style="width:180px">
												<c:if test="${entity.isExport == '' or entity.isExport == null }"><option value="0">不导出</option><option value="1" selected>导出</option></c:if>
                                                <c:if test="${entity.isExport == '0' }"><option value="0" selected>不导出</option><option value="1">导出</option></c:if>
                                            	<c:if test="${entity.isExport == '1' }"><option value="0">不导出</option><option value="1" selected>导出</option></c:if>
											</select>
										</td>
									</tr>
									<tr>
										<td class="kv-label">是否打印：</td>
										<td class="kv-content">
											<select id="isPrint" name="isPrint" style="width:180px">
												<c:if test="${entity.isPrint == '' or entity.isPrint == null }"><option value="0">不打印</option><option value="1" selected>打印</option></c:if>
                                                <c:if test="${entity.isPrint == '0' }"><option value="0" selected>不打印</option><option value="1">打印</option></c:if>
                                            	<c:if test="${entity.isPrint == '1' }"><option value="0">不打印</option><option value="1" selected>打印</option></c:if>
											</select>
										</td>
									</tr>
									<tr>
										<td class="kv-label">是否显示：</td>
										<td class="kv-content">
											<select id="isShow" name="isShow" style="width:180px">
												<c:if test="${entity.isShow == '' or entity.isShow == null }"><option value="0">不显示</option><option value="1" selected>显示</option></c:if>
                                                <c:if test="${entity.isShow == '0' }"><option value="0" selected>不显示</option><option value="1">显示</option></c:if>
                                            	<c:if test="${entity.isShow == '1' }"><option value="0">不显示</option><option value="1" selected>显示</option></c:if>
											</select>
										</td>
									</tr>
									
									
								</table>
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>

<script type="text/javascript">
//@ sourceURL=templet_tablecustom_form
	$(function() {
		$('#mainformCustom').form({
			onSubmit : function() {
				var isValid = $(this).form('validate');
				// 返回false终止表单提交
				return isValid;
			},
			success : function(data) {
				var dataObj = eval("(" + data + ")");//转换为json对象
				if (submitSuccess(dataObj, dgCourse, dCourse)) {
					dgCourse.datagrid('reload');
				}
			}
		});
		
	});
	
</script>
</body>
</html>